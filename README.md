# Countries of the World

This was an coding challenge that was presented to me recently and should
demonstrate some of my capabilites. Obviously this application doesn't showcase
my entire skillset but should provde some insight in to my approach and coding
style. The problem statement was as follows:

*Given the pulic API - https://restcountries.com create an application that
leverages the various endpoints to display a list of countries and country
details. The design and behavior is left in the hands of the implementor to
best show off their skills and ability.*

Please note that the searching capabilities of the API leaves some room for
improvement. Namely that doing partial searches for a country will yield some
incomplete results.

## Installation

The applicaiton is based upon create-react-app and follows the same
installation steps:

```bash
npm install
```

## Running the application

Once installation is complete, then the application can be started via:

```bash
npm start
```
