import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import {QueryClient, QueryClientProvider} from "react-query";
import {Countries} from "./Countries";
import {ErrorPage} from "./ErrorPage";

const queryClient = new QueryClient({
   defaultOptions: {
      queries: {
         retry: false
      }
   }
});

const App = (): React.ReactNode =>
    <QueryClientProvider client={queryClient}>
        <Router>
            <Routes>
                <Route path='/' element={<Countries/>}/>
                <Route path='*' element={<ErrorPage/>}/>
            </Routes>
        </Router>
    </QueryClientProvider>;

export default App;
