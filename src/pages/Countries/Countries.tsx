import React, {useState, useEffect} from "react";
import {useSearchParams} from "react-router-dom";
import './Countries.css';
import {CountryCard} from "../../components/CountryCard";
import {useCountries} from "../../hooks/useCountries";
import {Country} from "../../hooks/useCountry";
import {CountryModal} from "../../components/CountryModal";

const Countries: React.FC = () => {
   const [searchParams, setSearchParams] = useSearchParams();
   const [searchFilter, setSearchFilter] = useState("");
   const [showModal, setModalPopup] = useState("");
   const {data, isLoading, isError, error} = useCountries(searchFilter);

   useEffect(() => {
      setSearchFilter(searchParams.get("search") ?? "");
   }, []);

   const handleSearchFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      setSearchFilter(e.target.value);
      setSearchParams({search: e.target.value});
   };

   return (
      <div className="countries">
         <div className="countries__input-container">
            <label htmlFor="search">Filter:</label>
            <input name="search" type="text" value={searchFilter} onChange={handleSearchFilterChange}/>
         </div>
         { isLoading && <div><h1>Loading...</h1></div> }
         { isError && <div><h1>Error: {error?.message}</h1></div> }
         { data && 
         <div className="countries__list-container">
            { data?.map((country: Country) =>
            <div onClick={() => setModalPopup(country.name.common) }>
               <CountryCard key={country.name.common} country={country} />
            </div>
            )}
         </div>
         }
         { showModal !== "" && <CountryModal country={showModal} handleClick={ () => setModalPopup("") }/> }
      </div>
   );
};
export default Countries;
