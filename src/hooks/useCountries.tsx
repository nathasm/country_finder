import {useQuery} from "react-query";

import {Country} from "./useCountry";
export const ALL_COUNTRIES_ENDPOINT = 'https://restcountries.com/v3.1/all';
export const QUERY_COUNTRY_BY_NAME_ENDPOINT = 'https://restcountries.com/v3.1/name'

const fetchCountries = async (filter: string | undefined): Promise<Country[]> => {
   const response = await fetch(filter ? `${QUERY_COUNTRY_BY_NAME_ENDPOINT}/${filter}` : ALL_COUNTRIES_ENDPOINT);
   if (response.status === 404) {
      throw new Error("Unknown country");
   }
   if (!response.ok) {
      throw new Error("Error contacting endpoint");
   }
   return response.json();
}

export const useCountries = (filter: string | undefined) => {
   return useQuery<Country[], Error>(['countries', filter], () => fetchCountries(filter));
};
