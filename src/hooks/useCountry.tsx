import {useQuery} from "react-query";
import {QUERY_COUNTRY_BY_NAME_ENDPOINT} from "./useCountries";

export type Country = {
   name: { common: string, official: string },
   flags: { svg: string }
}
export type CountryDetail = Country & {
   region?: string,
   subregion?: string,
   capital?: string,
   population?: number,
};

type CountryResponse = Array<Country>;

const fetchCountry = async (id: string | undefined): Promise<CountryDetail> => {
   if (id === undefined) {
      throw new Error("Invalid search string");
   }
   const response = await fetch(`${QUERY_COUNTRY_BY_NAME_ENDPOINT}/${id}`);
   if (!response.ok) {
      throw new Error("Error contacting endpoint");
   }
   const responseToJson: CountryResponse = await response.json();
   return responseToJson[0];
}

export const useCountry = (search: string | undefined) => {
   return useQuery(['countries', search], () => fetchCountry(search));
};
