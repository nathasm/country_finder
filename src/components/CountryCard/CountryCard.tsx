import React from 'react';

import './CountryCard.css';
import {Country} from "../../hooks/useCountry";

type CountryCardProps = {
   country: Country
}

const CountryCard: React.FC<CountryCardProps> = ({ country }: CountryCardProps) => 
   <div className="country-list-card">
      <img className="country-list-card__img" src={country.flags.svg} alt={`${country.name.common} flag`}/>
      <span className="country-list-card__name">{country.name.common}</span>
   </div>;

export default CountryCard;
