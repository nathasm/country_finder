import React from 'react';
import './CountryModal.css';
import {useCountry} from "../../hooks/useCountry";

interface CountryModalProps {
   country: string;
   handleClick: () => void;
}

const CountryModal: React.FC<CountryModalProps> = ({ country, handleClick }: CountryModalProps) => {
   const {data, isLoading, isError, error} = useCountry(country);
   if (isLoading) {
      return <div><h1>Loading...</h1></div>;
   }
   if (isError) {
      return <div><h1>An error occurred when fetching country data - {error}</h1></div>;
   }
   return data ? <div className="modal country-modal" onClick={handleClick}>
      <div className="country-card">
         <img className="country-card__img" src={data.flags.svg} alt={`${country} flag`}/>
         <h1>{data.name.common}</h1>
         <div><span>Region:</span> <span>{data?.region ?? ''}</span></div>
         <div><span>Capital:</span> <span>{data?.capital?.[0] ?? ''}</span></div>
         <div><span>Population:</span> <span>{data?.population ?? 0}</span></div>
      </div>
      </div> : null;
};
export default CountryModal;
